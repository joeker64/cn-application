"""
Copyright © 2023 Arm Ltd and Contributors. All rights reserved.
SPDX-License-Identifier: Apache-2.0
"""

"""
main.py:
    Starts flask server to host local webpage. Reads directly from video source and uses gRPC service "getStream" to send frame data
    and recive bounding boxes, scores and class identifies
"""
import requests
import numpy as np
import json
import flask
from flask import Flask, render_template, Response, request, redirect
import cv2
import argparse
import time
import logging
from src.read_stream import ReadFromRTSP, ReadFromRTMP
from src.argconfig import ArgConf
from src.const import *
from src.camera import get_camera_object
from src.inference import send_for_inference, start_grpc_connection
from src.utils import preprocess_frame, get_class_list
from src.postprocess import postprocess_results

ap = argparse.ArgumentParser()
args = ArgConf(ap)

app = Flask(__name__)

model = args["model"]
names = get_class_list(args)

camera = get_camera_object(args)
if not camera.isOpened():
    print("Could not open camera. Please check camera or rtsp path")
    camera.release()
    exit()

cloud_stream = None
if args["cloud_rtmp_stream"] != "":
    cloud_stream = ReadFromRTMP(args["cloud_rtmp_stream"])
    cloud_stream.start()

frame_count = 0
frame_read_time_avg = 0
preprocess_time_avg = 0
inference_time_avg = 0
postprocess_time_avg = 0
cloud_metrics_time_avg = 0
pure_inf_time_avg = 0
rest_inf_time_avg = 0
normal_cam_read =0
pipeline_time_avg = 0
pure_inf_time = 0

def no_ui_inference():
    """
    Starts the application process without the flask web page

    Args:
        None
    
    Returns:
        None
    """
    global person, bicycle, car, bus, truck, total_count
    global rest_inf_time, pure_inference_time
    global pipeline_time_avg, normal_cam_read, frame_read_time_avg, preprocess_time_avg, inference_time_avg, postprocess_time_avg, cloud_metrics_time_avg, frame_count, pure_inf_time_avg, rest_inf_time_avg
    inference_conn = start_grpc_connection(args["ipaddr"], args["inf_port"])

    while(camera.isOpened()):
        start = time.time()
        ret, frame = camera.read()  
        normal_cam_read += time.time() - start    
        frame_read_time = camera.frame_read_time

        if frame is None:
            print('End of video not detected')
            exit()

        prepro_start_time = time.time()
        img = preprocess_frame(frame)
        preprocess_time = time.time() - prepro_start_time
        
        inf_start = time.time()
        right_boxes, right_classes, right_scores, model_load_time, pure_inference_time = send_for_inference(img, inference_conn)
        inference_time = time.time() - inf_start

        postpro_start = time.time()
        arr, img = postprocess_results(right_boxes, right_classes, right_scores, frame, img, names,  model)

        person, bicycle, car, bus, truck= arr[0], arr[1], arr[2], arr[3], arr[4]
        total_count = arr[0] + arr[1] + arr[2] + arr[3] + arr[4]
        
        postprocess_time = time.time() - postpro_start


        if args["cloud_address"] != "":
            cloud_info_start = time.time()
            image_string = cv2.imencode('.jpg', img)[1]
            image_tensor = image_string.tolist()
            iot_resp = {"cloud_metrics":{"person": person, "bicycle": bicycle, "car": car, "bus": bus, "truck": truck,
                        "pure_fps": pure_inference_time, "end_to_end_fps": rest_inf_time, "total_count": total_count}, 
                        "image": image_tensor,
                        "app_time": {"Start_time": 0, "Cloud_start": 0}}
                        # "app_time": {"Start_time": frame_time, "Cloud_start": cloud_info_start}}
            resp = requests.post("http://"+ args["cloud_address"] +"/video",json=iot_resp)
            resp = json.loads(resp.text)
            # cloud_metrics_time = time.time() - cloud_info_start
            cloud_metrics_time =  resp["End_to_End_pipeline_time"] - cloud_info_start
            rest_inf_time = 1 / (resp["End_to_End_pipeline_time"] - start)
            pipeline_time = resp["End_to_End_pipeline_time"] - start
        else: 
            end = time.time()
            rest_inf_time = 1 / (end - start)
            pipeline_time = end - start
        
        frame_count += 1
        if frame_count % 10 == 0:    
            print("--------------------------TIME TAKEN REPORT-------------------------------")
            print("Video reading thread : ", frame_read_time_avg / 10)
            print("Preprocess time    : ", preprocess_time_avg / 10)
            print("Inference time     : ", inference_time_avg / 10)
            print("Postprocess time   : ", postprocess_time_avg / 10)
            frame_count = 0
            frame_read_time_avg = 0
            preprocess_time_avg = 0
            inference_time_avg = 0
            postprocess_time_avg = 0
            normal_cam_read = 0

            if args["cloud_address"] != "":
                print("Cloud metrics time : ", cloud_metrics_time_avg / 10)
                cloud_metrics_time_avg = 0
            print("Total time taken     : ", pipeline_time_avg / 10)
            print("END_TO_END_FPS     : ", rest_inf_time_avg / 10)
            print("Pure inference FPS : ", pure_inf_time_avg / 10)
            rest_inf_time_avg = 0
            pure_inf_time_avg = 0
            pipeline_time_avg = 0
            print("-----------------------------END-------------------------------------------")
        else:
            frame_read_time_avg += frame_read_time
            preprocess_time_avg += preprocess_time
            inference_time_avg += inference_time
            postprocess_time_avg += postprocess_time
            pipeline_time_avg += pipeline_time
            if args["cloud_address"] != "":
                cloud_metrics_time_avg += cloud_metrics_time
            rest_inf_time_avg += rest_inf_time
            pure_inf_time_avg += pure_inference_time
        print("--------------------Inference results-------------------")
        print("Detected objects : ")
        print(f"Person : {person}, Car:{car}, Bicycle: {bicycle}, Truck: {truck}, Bus: {bus}")
        print("FPS stats :")
        print(f"Pure inference time: {pure_inference_time}, End to End Inference time: {rest_inf_time}")
        print("---------------------------------------------------------")


def gen_frames():
    """
    Starts the application process with the flask web page

    Args:
        None

    Returns:
        None
    """
    global person, bicycle, car, bus, truck, total_count
    global rest_inf_time, pure_inference_time
    global pipeline_time_avg, normal_cam_read, frame_read_time_avg, preprocess_time_avg, inference_time_avg, postprocess_time_avg, cloud_metrics_time_avg, frame_count, pure_inf_time_avg, rest_inf_time_avg
    inference_conn = start_grpc_connection(args["ipaddr"], args["inf_port"])

    while(camera.isOpened()):

        start = time.time()
        ret, frame = camera.read()  
        normal_cam_read += time.time() - start    
        frame_read_time = camera.frame_read_time

        if frame is None:
            print('End of video/video not detected')
            exit()

        prepro_start_time = time.time()
        img = preprocess_frame(frame)
        preprocess_time = time.time() - prepro_start_time
        
        inf_start = time.time()
        right_boxes, right_classes, right_scores, model_load_time, pure_inference_time = send_for_inference(img, inference_conn)
        inference_time = time.time() - inf_start

        postpro_start = time.time()
        arr, img = postprocess_results(right_boxes, right_classes, right_scores, frame, img, names,  model)
        
        if cloud_stream is None:
            image_string = cv2.imencode('.jpg', img)[1]
            image_tensor = image_string.tobytes()
            yield (b'--frame\r\n'b'Content-Type: image/jpeg\r\n\r\n' + image_tensor + b'\r\n')
        else:
            ret, cloud_frame = cloud_stream.read()
            if not(cloud_frame is None):
                cl_image_str = cv2.imencode('.jpg', cloud_frame)[1]
                cl_image_byt = cl_image_str.tobytes()
                yield (b'--frame\r\n'b'Content-Type: image/jpeg\r\n\r\n' + cl_image_byt + b'\r\n')

        person, bicycle, car, bus, truck= arr[0], arr[1], arr[2], arr[3], arr[4]
        total_count = arr[0] + arr[1] + arr[2] + arr[3] + arr[4]
        
        postprocess_time = time.time() - postpro_start
                
        if args["cloud_address"] != "":
            cloud_info_start = time.time()
            image_string = cv2.imencode('.jpg', img)[1]
            image_tensor = image_string.tolist()
            iot_resp = {"cloud_metrics":{"person": person, "bicycle": bicycle, "car": car, "bus": bus, "truck": truck,
                        "pure_fps": pure_inference_time, "end_to_end_fps": rest_inf_time, "total_count": total_count}, 
                        "image": image_tensor,
                        "app_time": {"Start_time": 0, "Cloud_start": 0}}
                        # "app_time": {"Start_time": frame_time, "Cloud_start": cloud_info_start}}
            resp = requests.post("http://"+ args["cloud_address"] +"/video",json=iot_resp)
            resp = json.loads(resp.text)
            cloud_metrics_time = time.time() - cloud_info_start
            # cloud_metrics_time =  resp["End_to_End_pipeline_time"] - cloud_info_start
            rest_inf_time = 1 / (resp["End_to_End_pipeline_time"] - start)
            pipeline_time = resp["End_to_End_pipeline_time"] - start
        else: 
            end = time.time()
            rest_inf_time = 1 / (end - start)
            pipeline_time = end - start
        
        frame_count += 1
        if frame_count % 10 == 0:    
            print("--------------------------TIME TAKEN REPORT-------------------------------")
            print("Video reading thread : ", frame_read_time_avg / 10)
            print("Preprocess time    : ", preprocess_time_avg / 10)
            print("Inference time     : ", inference_time_avg / 10)
            print("Postprocess time   : ", postprocess_time_avg / 10)
            frame_count = 0
            frame_read_time_avg = 0
            preprocess_time_avg = 0
            inference_time_avg = 0
            postprocess_time_avg = 0
            normal_cam_read = 0

            if args["cloud_address"] != "":
                print("Cloud metrics time : ", cloud_metrics_time_avg / 10)
                cloud_metrics_time_avg = 0
            print("Total time taken     : ", pipeline_time_avg / 10)
            print("END_TO_END_FPS     : ", rest_inf_time_avg / 10)
            print("Pure inference FPS : ", pure_inf_time_avg / 10)
            rest_inf_time_avg = 0
            pure_inf_time_avg = 0
            pipeline_time_avg = 0
            print("-----------------------------END-------------------------------------------")
        else:
            frame_read_time_avg += frame_read_time
            preprocess_time_avg += preprocess_time
            inference_time_avg += inference_time
            postprocess_time_avg += postprocess_time
            pipeline_time_avg += pipeline_time
            if args["cloud_address"] != "":
                cloud_metrics_time_avg += cloud_metrics_time
            rest_inf_time_avg += rest_inf_time
            pure_inf_time_avg += pure_inference_time
            

# Sending all the metrics to the WEB UI using flask
@app.route('/total_count')
def total_count():
    def update():
        global total_count
        yield f'data: {total_count}\n\n'
    return flask.Response(update(), mimetype='text/event-stream')


@app.route('/learn_1d')
def learn_1d():
    def update():
        global person
        yield f'data: {person}\n\n'
    return flask.Response(update(), mimetype='text/event-stream')


@app.route('/learn_2d')
def learn_2d():
    def update():
        global car
        yield f'data: {car}\n\n'
    return flask.Response(update(), mimetype='text/event-stream')


@app.route('/learn_3d')
def learn_3d():
    def update():
        global bus
        yield f'data: {bus}\n\n'
    return flask.Response(update(), mimetype='text/event-stream')


@app.route('/learn_4d')
def learn_4d():
    def update():
        global truck, prev_count, frame_count
        yield f'data: {truck}\n\n'
    return flask.Response(update(), mimetype='text/event-stream')


@app.route('/learn_5d')
def learn_5d():
    def update():
        global bicycle, prev_count, frame_count
        yield f'data: {bicycle}\n\n'
    return flask.Response(update(), mimetype='text/event-stream')


@app.route('/rest_fps')
def rest_fps():
    def update():
        global rest_inf_time
        yield f'data: {rest_inf_time:.3f}\n\n'
    return flask.Response(update(), mimetype='text/event-stream')


@app.route('/pure_fps')
def pure_fps():
    def update():
        global pure_inference_time
        yield f'data: {pure_inference_time}\n\n'
    return flask.Response(update(), mimetype='text/event-stream')


@app.route('/video_feed')
def video_feed():
    return Response(gen_frames(), mimetype='multipart/x-mixed-replace; boundary=frame')


# Function to render the HTML page in the WEB Browser.... The user can select the number of devices to be added to the web ui using below code snippets
@app.route('/')
def index():
    return render_template('index.html', device_1=device_1)

@app.route('/video_server')                                                                                                                           
def video_server():
    print(args["cloud_address"])                                                                                                                                   
    return redirect("http://" + args["cloud_address"])

if __name__ == '__main__':
    if args["disable_ui"]:
        no_ui_inference()
    else:
        app.run(host="0.0.0.0")    
           
