"""
Copyright © 2023 Arm Ltd and Contributors. All rights reserved.
SPDX-License-Identifier: Apache-2.0
"""
import cv2
from src.read_stream import ReadFromRTSP

def get_camera_object(args):
    """
    Get camera source based on user configuration

    Args:
        args (argparse.ArgumentParser): Argument parser class
    Returns:
        camera (cv2.VideoCapture | src.ReadFromRTSP): Video output class 
    """
    # using subprocess and pipe to fetch frame data
    if args["rtsp"] == "":
        print("Taking input from local USB camera....")
        camera = cv2.VideoCapture(args["cam_path"])
    else:
        print("Taking input from RTSP stream....")
        camera = ReadFromRTSP(args["rtsp"])
        camera.start()
    return camera