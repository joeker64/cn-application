"""
Copyright © 2023 Arm Ltd and Contributors. All rights reserved.
SPDX-License-Identifier: Apache-2.0
"""

def ArgConf(ap):
    """
    Provides command line arguments to main.py

    Args:
        ap (argparse.ArgumentParser): Argument parser class

    Returns:
        args (argparse.ArgumentParser): Argument parser class with added arguments
    """
    ap.add_argument("-ip", "--ipaddr", default="127.0.0.1", help="ip address the device where inference container is running")
    ap.add_argument("-csp", "--cloud_address", default="", help="Cloud container ip and port number eg. 127.0.0.1:8081")
    ap.add_argument("-csp_s", "--cloud_rtmp_stream", default="", help="RTMP pull url for fetching the stream")
    ap.add_argument("-p", "--inf_port", default="8080", help="inference container port number")
    ap.add_argument("-c", "--cam_path", help="Camera device Path")
    ap.add_argument("-r", "--rtsp", default="", help="RTSP stream link")
    ap.add_argument("-m", "--model", required=True, help="Model upon which inference is done")
    ap.add_argument("-nui", "--disable_ui", help="Disable Web UI integration") 
    ap.add_argument("-cloud","--cloud_service", help="aws/ailbaba")     
    args = vars(ap.parse_args())
    return args