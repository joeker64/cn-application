"""
Copyright © 2023 Arm Ltd and Contributors. All rights reserved.
SPDX-License-Identifier: Apache-2.0
"""

"""
img_helper.py:
    Draws boxes on image to show the output of an inference model 
"""
import numpy as np
import cv2

def draw_bounding_boxes_yolo(frame, boxes, scores, classes, names, img_shape):
    """
    Uses OpenCV to draw boxes highlighing when using Tiny YoloV3 as our inference model

    Args:
        img (cv2.Mat): Frame to draw boxes on
        bounding_boxes (numpy Array): Coordinates of the boxes highlighting the classes
        scores (numpy.Array): Scores corresponding to the "bounding_boxes"
        classes (numpy.Array): Names of the ojects corresponding to the "bounding_boxes"
        names (string list): All possible classes that can be detected
        img_shape (int list): shape/properties of the frame
    Returns:
        frame (cv2.Mat): Frame in which inference data has been drawn on
    """
    class_i = 0
    for bbox in boxes:
        text = names[int(classes[class_i])].title()
        color_codes = [[255, 128, 63], [71, 217, 5], [199, 35, 0] , [157, 7, 248] , [255, 255, 0]]
        (x, y), base = cv2.getTextSize(text, cv2.FONT_HERSHEY_DUPLEX, 0.8, 2)
        xmin = (bbox[0]/img_shape[1])*frame.shape[1]
        ymin = (bbox[1]/img_shape[0])*frame.shape[0]
        xmax = (bbox[2]/img_shape[1])*frame.shape[1]
        ymax = (bbox[3]/img_shape[0])*frame.shape[0]
        xmin, ymin, xmax, ymax = int(xmin), int(ymin), int(xmax), int(ymax)
        frame = cv2.rectangle(frame, (xmin, ymin - y - base), (xmin + x+20, ymin+10), color_codes[classes[class_i]], -1)
        cv2.rectangle(frame, (xmin, ymin), (xmax, ymax), color_codes[classes[class_i]], 2)
        frame = cv2.putText(frame, text, (xmin+10, ymin), cv2.FONT_HERSHEY_DUPLEX, 0.8, (0, 0, 0), 2, cv2.LINE_AA)
        class_i += 1
    return frame


def postprocess_img(img, img_size, bond_boxes=None):
    """
    Prepare the Frame by resizing the image and Ensuring box coordinates use float type
    
    Args:
        img (cv2.Mat): Frame to resize
        img_size (int array): X and Y coordinate of the frame
        bond_boxes (numpy.Array): Coordinates of the boxes highlighting the classes
    Returns:
        img_resized (cv2.Mat): resized frame
        bond_boxes (numpy.Array): Coordinates of the boxes (float type) highlighting the classes
    """
    img_h, img_w = img.shape[:2]
    width, height = img_size

    img_scale = min(img_w / width, img_h / height)
    new_w, new_h = int(img_scale * width), int(img_scale * height)
    dw, dh = (img_w - new_w) // 2, (img_h - new_h) // 2

    img = img[dh:new_h + dh, dw:new_w + dw, :]
    img_resized = cv2.resize(img, (width, height))

    if bond_boxes is None:
        return img_resized, None
    else:
        bond_boxes = bond_boxes.astype(np.float32)
        bond_boxes[:, [0, 2]] = np.clip((bond_boxes[:, [0, 2]] - dw) / img_scale, 0., width)
        bond_boxes[:, [1, 3]] = np.clip((bond_boxes[:, [1, 3]] - dh) / img_scale, 0., height)

        return img_resized, bond_boxes

def decode_class_names(classes_path):
    """
    Read class names from file

    Args:
        classes_path (string): Path to file containing classes
    Returns:
        clases (string array): List of all possible classes that can be detected by the inference model
    """
    with open(classes_path, 'r') as f:
        lines = f.readlines()
    classes = []
    for line in lines:
        line = line.strip()
        if line:
            classes.append(line)
    return classes

