"""
Copyright © 2023 Arm Ltd and Contributors. All rights reserved.
SPDX-License-Identifier: Apache-2.0
"""

"""
postprocess.py:
    Any computation needed before displaying on ui
"""
from src.utils import get_final_class_count
from src.img_helper import draw_bounding_boxes_yolo, postprocess_img
import cv2
import numpy as np

def postprocess_results(right_boxes, right_classes, right_scores, frame, img, names, model):
    """
    Draw output of the inference container (boxes, scores & class names) onto the frame

    Args:
        right_boxes (int array): Coordinates of boxes highlighting classes detected in the frame
        right_classes (string array): Names of classes identified in the frame 
        right_scores (float array): Scores of accuracy of the identification of classes in the frame
        frame (cv2.Mat): Raw frame from video source
        img (cv2.Mat): Adjusted frame from video source
        names (string array): List of all possible classes that can be detected in the model
        model (string): Name of model used in inference. Currently suppored models are "yolov3" & "tiny_yolov3"
    
    Returns:
        arr (int array): Total count for each class that has been detected
        img (cv2.Mat): Frame with inference data drawn onto
    """
    if right_boxes == 0:
        return [0,0,0,0,0], frame
    else:
        right_boxes = np.array(right_boxes)
        right_classes = np.array(right_classes)
        right_scores = np.array(right_scores)
        img, right_boxes = postprocess_img(img, frame.shape[:2], right_boxes)
        arr = get_final_class_count(right_classes, model)
        if right_boxes is not None:
            if model == "yolov3" or model == "tiny_yolov3":
                img = draw_bounding_boxes_yolo(frame, right_boxes, right_scores, right_classes, names, img.shape[:2])       
        return arr, img