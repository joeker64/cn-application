"""
Copyright © 2023 Arm Ltd and Contributors. All rights reserved.
SPDX-License-Identifier: Apache-2.0
"""

"""
const.py:
    Sets enviroment varibles for main.py
"""
import json
# If the user wants to skip frames, make the below variable as true and specify the number of frames to be skipped before the next inference.
frame_skip = False
skip_frame_counter = 60

# Taking the configuration from config file
config_file = open("config/config.json")
config = json.load(config_file)

# Taking the device name and the preprocess container link from the config file.
# The user can use this snippet to configure the number of devices to be added to the WEB UI.

device_1 = config["Device_1"]

# for adding 3rd device uncomment below line.
# Note that an entry in config/config.json should be created before uncommenting.
# device_3 = config["Device_3"]
# dev_link_3 = config["Dev_link_3"]

# for adding 4th device uncomment below line.
# Note that an entry in config/config.json should be created before uncommenting.
# device_4 = config["Device_4"]
# dev_link_4 = config["Dev_link_4"]

# Initializing the detection metrics and fps parameters to zero 
total_count = 0
pure_inference_time = 0
rest_inf_time = 0
car = 0
bicycle = 0
truck = 0
bus = 0
person = 0
frame_count = -1

frame_count = 0
frame_time_avg = 0
preprocess_time_avg = 0
inference_time_avg = 0
postprocess_time_avg = 0
cloud_time_avg = 0
end_to_end_pipeline_avg = 0