"""
Copyright © 2023 Arm Ltd and Contributors. All rights reserved.
SPDX-License-Identifier: Apache-2.0
"""

"""
utils.py:
    Small funcitons that provide basic tasks
"""

from src.img_helper import decode_class_names
import cv2
def get_final_class_count(classes, model):
    """
    Gets the total count for each class
    
    Args:
        classes (string list): List of all possible classes that can be detected in the model
        model (string): Model being using in inference. Supported models are yolov3 or tiny_yolov3

    Returns:
        arr (int array): Total count for each class
    """
    arr = [0] * 5
    for i in list(classes):
        if model == "yolov3" or model == "tiny_yolov3":
            arr[i] = arr[i] + 1
    return arr

def preprocess_frame(frame):
    """
    Adjusts frame before being sent for inference

    Args:
        frame (cv2.Mat): Raw frame from video source
    Returns:
        frame (cv2.Mat): Adjusted frame from video source
    """
    frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
    frame = cv2.resize(frame, (416,416))    
    return frame


def get_class_list(args):
    """
    Reads all classes that can be detected from the model from file

    Args:
        args (argparse.ArgumentParser): Argument parser class
    
    Returns:
        names (string list): List of all possible classes that can be detected from the model
    """
    # Initializing the model specific processing and files
    class_name_path = "model_data/class_file.txt"
    names = decode_class_names(class_name_path)

    return names