"""
Copyright © 2023 Arm Ltd and Contributors. All rights reserved.
SPDX-License-Identifier: Apache-2.0
"""

"""
read_stream.py:
    Streaming video data from a remote source
"""
import cv2
import time
from threading import Thread

# Thread to read fraggme from RTSP stream
class ReadFromRTSP(Thread):
    def __init__(self, rtsp_src):
        #Utilise cv2 to read from rtsp source
        Thread.__init__(self)
        self.stream = cv2.VideoCapture(rtsp_src)
        
        if not self.stream.isOpened():
            print("Could not intitialize the RTSP stream.....")
            exit()
        
        self.stream.set(cv2.CAP_PROP_BUFFERSIZE, 1)
        self.status , self.frame = self.stream.read()
        while not self.status:
            self.status, self.frame = self.stream.read()
        self.stop = False
        self.frame_read_time = 0


    def stop(self):
        self.stop = True

    def run(self):
        #Start streaming data
        while(self.stream.isOpened()):
            self.frame_read_start = time.time()
            self.status, self.frame = self.stream.read()
            if self.frame is None:
                continue
            #print(self.frame)
            frame_read_end = time.time()
            self.frame_read_time = frame_read_end - self.frame_read_start
            if (self.frame is None) or (self.stop == True):
                print("Stream stopped / Connection broken.......")
                break
    def read(self):
        return self.status, self.frame
    def isOpened(self):
        return self.stream.isOpened()
    def get(self, id):
        return self.stream.get(id)

# Thread to read fraggme from RTMP stream
class ReadFromRTMP(Thread):
    def __init__(self, rtmp_src):
        Thread.__init__(self)
        self.rtmp_src = rtmp_src
        self.stop = False
        self.status = False
        self.frame = None

    def connect_stream(self):
        time.sleep(20)
        try:
            self.stream = cv2.VideoCapture(self.rtmp_src)
        except cv2.error as e:
            print("Connecting to RTMP....")

        while not self.stream.isOpened():
            try:
                self.stream = cv2.VideoCapture(self.rtmp_src) 
            except cv2.error as e:
                print("Reconnecting......")
 
        if not self.stream.isOpened():
            print("Could not intitialize the RTMP stream.....")
        
        self.status , self.frame = self.stream.read()
        while not self.status:
            self.stream = cv2.VideoCapture(self.rtmp_src)
            self.status, self.frame = self.stream.read()
            print("getting the frame....")

    def run(self):
        self.connect_stream()
        while(True):
            self.status, self.frame = self.stream.read()
            if (self.frame is None) or (self.stop == True):
                self.connect_stream() 
                print("Stream stopped / Connection broken.......")

    def read(self):
        return self.status, self.frame
    def isOpened(self):
        return self.stream.isOpened()