"""
Copyright © 2023 Arm Ltd and Contributors. All rights reserved.
SPDX-License-Identifier: Apache-2.0
"""

"""
inference.py:
    Gathers the data from the inference container using gRPC service "MainServer" for channel "getStream"
"""
import cv2
import grpc
from src.grpc_files import Datas_pb2
from src.grpc_files import Datas_pb2_grpc
import json

def _Request(frame):
    """
    Encode frame to jpeg wrap it in the gRPC message "Request"

    Args:
        frame (cv2.Mat): Frame from video source

    Returns:
        (grpc.Request): Frame data in custom grpc message format "Request"
    """
    ret, buf = cv2.imencode('.jpg', frame)
    if ret != 1:
        return
    yield Datas_pb2.Request(datas=buf.tobytes())

def start_grpc_connection(inf_ip, inf_port):
    """
    Create client for grpc service for "MainServer" on channel "getStream"

    Args:
        inf_ip (string): Address of the inference container
        inf_port (string): Port on which the inference container is publishing too

    Returns:
        stub (grpc.MainServerStub): gRPC object for communication with server
    """
    channel = grpc.insecure_channel(inf_ip + ":" + inf_port)
    stub = Datas_pb2_grpc.MainServerStub(channel)
    return stub

def send_for_inference(img, stub):
    """
    Send frame to inference container via gRPC

    Args:
        img (cv2.Mat): Frame from video source
        stub (grpc.MainServerStub): gRPC object for communication with server

    Returns:
        right_boxes (int array): Coordinates of boxes highlighting classes detected in the frame
        right_classes (string array): Names of classes identified in the frame 
        right_scores (float array): Scores of accuracy of the identification of classes in the frame
        model_load_time (float): How long it took for the model to be loaded in the inference container
        pure_inference_time (float): How long it took the inference to be ran on the frame
    """
    image_string = cv2.imencode('.jpg', img)[1]
    image_tensor = image_string.tolist()
    resp = stub.getStream(_Request(img))
    for i in resp:
        res = i.reply
        data = json.loads(res)
        right_boxes, right_classes, right_scores = data["right_boxes"], data["right_classes"], data["right_scores"]
        model_load_time = data["model_load"]
        pure_inference_time = float("{:.3f}".format(float(data["inf_time"])))
        
    return right_boxes, right_classes, right_scores, model_load_time, pure_inference_time