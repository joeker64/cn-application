FROM python:3.7-slim-buster
ARG DEBIAN_FRONTEND=noninteractive
RUN apt-get update
RUN apt install -y libgl1-mesa-glx
RUN apt install -y libglib2.0-0
RUN apt install -y gcc
RUN apt-get install -y ffmpeg

RUN python3 -m pip install --upgrade pip
RUN python3 -m pip install opencv-python
RUN python3 -m pip install requests
RUN python3 -m pip install flask
RUN python3 -m pip install flask_ngrok
RUN python3 -m pip install grpcio grpcio-tools
RUN python3 -m pip install backports.zoneinfo

COPY static/ static/
COPY model_data/ model_data/
COPY config config/
COPY templates/ templates/

COPY src src/
WORKDIR src/grpc_files

CMD python gen.py
WORKDIR /

COPY main.py main.py
ENTRYPOINT ["python3","main.py"]
