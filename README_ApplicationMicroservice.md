
# Building and Deploying Application Container

## Introduction
This document guides the user to build and deploy application container.

## Pre-Requisites
- ARM Based Embedded Platform.
    - ARM System Ready Device
- USB Pen Drive Flashed with ARM System-Ready EWOAL Image with preinstalled docker.

## Clone the repository

- Clone the gitlab repo using below command
```
$ git clone -b <BRANCH_NAME> <PROJECT_GIT_URL>
```

## Deployment of Application container
Follow the steps given below for setting up the container,
- Go to the application container folder
```sh
$ cd <PROJECT_ROOT_FOLDER>
```
- Open the config/config.json file in an editor and update the device name and device link. 
- The device link should be provided in the below format. 
  - Note that device 1 will be considered as primary device on web UI please update the name accordingly. That is device 1 should be the device on which application container will be running 

```sh
“Dev_link_1” : “http://<IP_ADRESS_OF_DEVICE>:5000” 
```
- Save the file.
- Run the command given below for building the application container
```sh
# Mention the image name & tag name of your choice
$ docker build -t <IMAGE_NAME>:<TAG_NAME> .
# Check the Docker Images list with the command below
$ docker images
```
- Deploy the application container using the command below
    ```sh
    $  docker  run -it --network=host <IMAGE_NAME>:<TAG_NAME> -p  <INF_PORT> -c  <USB_CAMERA_PATH> -m <MODEL> -ip  <INFERENCE_CONTAINER_IP> -csp  <CLOUD_ADDRESS> -r  < RTSP_STREAM> -csp_s <CLOUD_RTMP_STREAM> -cloud <CLOUD_PLATFORM_OF_CSP>
    ```
    Arguments
    - -p: Port number where the ML inference container is running.
    - -m: Model name that should be loaded for ML inference. Currently yolov3 and tiny_yolov3 values are supported
    - -ip: This is an optional argument which is given if the inference container is deployed on the cloud. The user needs to provide the public IP of the Edge instance (An Edge instance is virtual machine that contains basic computing componentssuch as the CPU, memory, operating system, network bandwidth, and disks. Once created, you can customize and modify the configuration of an Edge instance.) as the argument.
    - -csp: Cloud container IPand port number like 127.0.0.1:8080
    - -csp_s: This is an optional argument which is given if we want to get live streaming from cloud then we can provide RTMP pull URL from Apsara Video Live as the argument.For more information, please refer to ApasraVideo live documentation. If we do not provide csp_s argument,then streaming will happen locally not from the cloud. 
    - -c: Path of the USBcamera device.
    - -r: This is also an optional argument which is provided to take input from RTSP (RealTime Streaming Protocol).Either -c or -r should be provided to take video input. And while taking RTSP input user must provide RTSP path as the argument.
    - -cloud: This argument is used to specify the cloud platform of csp container if it is running. Supported platforms are aws/alibaba. 

## Adding a new device to the WEB UI
This  step  is  optional,  If  the  user  needs  to  add  new  devices  to  the  flask  web  uithen proceed with the following steps.
- Go inside the templates/ directory inside  the  device  on which the application container has been built.
```sh
$ cd <PROJECT_ROOT>/templates
```
- Open the index.html file in an editor. Add  a  new  line  after  line  number  84  and  add  the  tab  for  new  device  in  the following format.Update the device number according to the number of devices  being added inthe snippet below (link_3 and device_3).
```sh
$ <li class="nav-item"><a href={{ link_3 }} target="_blank"class="nav-link second-tab-toggle">{{ device_3 }}</a></li>
```
- Go to the application container folder. 

- Open config.json in config/ folder and create a new entry for device name and device link. The user can refer to the entries created for ARM System Ready Device. 

- Open src/const.py 

  - Uncomment line number 20 and 21 for adding 3rd device 

  - For adding 4th device uncomment line number 25 and 26. 

- Open main.py in 

  - Uncomment line number 334 and 350 to add 3rd device to web UI OR uncomment line number 338 and 354 to add 3rd and 4th device to Web UI. 

  - Comment line number 330 and 346. 

The user can refer to the predefined device formats to add new device. 

Build and run the application container to see the newly added device and tab in the Web UI. 

## Build the cloud container
- Refer the document, ARM-Smart_camera_CSP_Alibaba_cloud_container_deployment.pdf
